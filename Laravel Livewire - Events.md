Laravel Livewire - Events
========================

## Dispatching

1. It is possible to dispatch Livewire/Alpine.js events from within a Livewire component by using the `$this->dispatch(...)` method

    ![qownnotes-media-bPkLIf](media/qownnotes-media-bPkLIf.png)

1. While doing so, it is also an option to add an additional payload, like with regular (JavaScript) events

    ![qownnotes-media-OiFEzP](media/qownnotes-media-OiFEzP.png)

1. Like it might be expected, this can also be done via a `$wire.dispatch(...)` method in the `@script`+ `@endscript` Blade/Livewire component directives

### The "Only" Approach

1. Using `$wire.dispatchSelf('...')` it is also possible to only dispatch an event only to the current Livewire component

1. Similarly, it is also possible to use `$this->dispatch('...')->self()` within the Livewire component

### In Alpine.js

1. Remember, Livewire components are essentially Alpine.js components

1. As this is the case, it is naturallz also possible to [dispatch Livewire events from within Alpine.js](https://livewire.laravel.com/docs/events#dispatching-livewire-events-from-alpine)

    ![qownnotes-media-FXrzOI](media/qownnotes-media-FXrzOI.png)

### Component To Component / Child To Parent

1. Making dispatching of an event for one specific other Livewire component possible can be done by `$this->dispatch('event-name', payload)->to(ComponentName::class)`

### In Blade

1. Another option would be to [dispatch a Livewire event in Blade](https://livewire.laravel.com/docs/events#dispatching-events-from-blade-templates) by using `wire:click="$dispatch(...)"`

1. Like with the component to component, it is also possible to use `wire:click="$dispatchTo('component-name', 'event-name', payload)`

## Listening

1. Within a Livewire component there is also a way to listen to events and trigger methods, using the PHP annotation `#[On(...)]`

    ![qownnotes-media-xbKmJq](media/qownnotes-media-xbKmJq.png)

1. This can also be defined by using a more dynamic approach by using a similar approach like the Laravel Route Model binding (*which is really nice*)

    ![qownnotes-media-pgGZuA](media/qownnotes-media-pgGZuA.png)

1. Like it might be expected, this can also be done via a `$wire.on(...)` method in the `@script`+ `@endscript` Blade/Livewire component directives

### The "Only" Approach

1. It is also possible to listen for an even by a [specific child Livewire component](https://livewire.laravel.com/docs/events#listening-for-events-from-specific-child-components)

    ![qownnotes-media-CIgpVu](media/qownnotes-media-CIgpVu.png)

### Global Listening

1. It is also possible to [listen to Livewire events on a global (`document`) scale](https://livewire.laravel.com/docs/events#listening-for-livewire-events-from-global-javascript)

    ![qownnotes-media-kdNRVR](media/qownnotes-media-kdNRVR.png)

### In Alpine.js

1. Remember, Livewire components are essentially Alpine.js components

1. As this is the case, it is naturally also possible to [listen for Livewire events within Alpine.js](https://livewire.laravel.com/docs/events#events-in-alpine)

    ![qownnotes-media-VUDhiB](media/qownnotes-media-VUDhiB.png)

## Testing

1. Luckily it is also possible to test for dispatched events within Feature Tests, using Pest / PhpUnit

1. This can be achieved by using `Livewire::test(...)` via the `Livewire\Livewire` Class

1. It is possible to, e.g., call actions and listen for fired events

    ![qownnotes-media-MCsGvc](media/qownnotes-media-MCsGvc.png)

1. The same is true for listening on Livewire components

    ![qownnotes-media-LVdpWQ](media/qownnotes-media-LVdpWQ.png)

## Real-time Eventing

(It might make sense to check this out with AMQP)

1. **For this to work, Laravel Echo / Laravel Reverb are required**

1.  For details and setup it is required to take a deeper look [into the official documentation](https://livewire.laravel.com/docs/events#listening-for-echo-events)

1. It is also possible to use [private and presence channels](https://livewire.laravel.com/docs/events#private--presence-channels)

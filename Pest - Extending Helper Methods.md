Pest - Extending Helper Methods
========================

1. Defining additional global helper methods within Pest is quite easy, as it is able to use `callStatic(...)` and `callStatic(...)`

1. Therefore, any method written within the `Pest.php` Class are automatically available within all the tests

1. There are several ideas that can directly be implemented from the get-go, like an reimplementation to the `actingAs(...)`or helper methods for repeated factory executions, but those are more depending on the project context

    ![qownnotes-media-kAXrHo](media/qownnotes-media-kAXrHo.png)

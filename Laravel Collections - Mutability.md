Laravel Collections - Mutability
========================

## Iterating over each item

### `map()`

1. `map()` is immutable

1. `map()` is going to return a new Collection instance


    ```php
    $newInstance = $collection->map(fn(Model $instance, string|int $index)) => $instance->slug = "syntactic-sugar-$index");
    ```


### `transform()`

1. `transform()` is mutable

1. `transform()` will change the existing Collection instance


    ```php
    $sameInstance = $collection->collection->transform(fn(Model $instance)) => $instance->withPermission());
    ```

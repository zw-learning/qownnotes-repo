Testing And Laravel Livewire
========================

> Livewire components are tested in isolation from each other, as [every component is its own island](https://livewire.laravel.com/docs/understanding-nesting#every-component-is-an-island)

## Content

1. If `Livewire` is installed and used it is possible to test against a specific Component and it's content

1. It is possible to, e.g., chain `assertSeeText([])` onto the `Livewire::test('componentClassName', [...])` call

1. Also, it is actually possible to assert and expect given states *in* a Livewire component, by assigning the PHPDOC of  `/** @test */` to a method

1. However, this is considered harmful to the code and quality of a component, so it's heavily advised against doing this

## Events And Listeners

1. Luckily it is also possible to test for dispatched events within Feature Tests, using Pest / PhpUnit

1. This can be achieved by using `Livewire::test(...)` via the `Livewire\Livewire` Class

1. It is possible to, e.g., call actions and listen for fired events

    ![qownnotes-media-MCsGvc](media/qownnotes-media-MCsGvc.png)

1. The same is true for listening on Livewire components

    ![qownnotes-media-LVdpWQ](media/qownnotes-media-LVdpWQ.png)

## Available Assertions

1. It is possible to get these information in full based on the [official documentation](https://livewire.laravel.com/docs/testing#all-available-testing-utilities)

1. Certain assertions are `assertSeeLivewire(ComponentName::class)`, `assertSee(...)`, `assertViewHas(...)`, `set('propertyName', 'value')` and  `assertSet('propertyName', 'value')`, 

    ![qownnotes-media-WdpQAe](media/qownnotes-media-WdpQAe.png)

1. If the `#[Url]` PHP annotation is set to a property, the use of `withQueryParams([...])` becomes useful, to react to specific URL query parameters

1. Besides the option to [check for cookies](https://livewire.laravel.com/docs/testing#calling-actions) it is also possible to call actions to assert their usage

    ![qownnotes-media-oJZJRy](media/qownnotes-media-oJZJRy.png)
    
    ![qownnotes-media-pzWLsh](media/qownnotes-media-pzWLsh.png)

1. If there are any errors it can also be asserted for

    ![qownnotes-media-xxSafc](media/qownnotes-media-xxSafc.png)
    
    ![qownnotes-media-BiKSdz](media/qownnotes-media-BiKSdz.png)

1. Same if there are issues with authorization, within the `updating(...)` Livewire lifecycle hook

    ![qownnotes-media-wMPVtw](media/qownnotes-media-wMPVtw.png)

1. Furthermore it is also possible to check for any redirect via `assertRedirect('routePath')`

## Missing Assertions

1. Even though Livewire is as nice and powerful to provide some useful methods in asserting the state of things, it is missing some crucial parts of Frontend validation

1. Wiring of Livewire component methods/actions within, e.g., a form is not part of it's current feature set

1. This is why the much known [Christoph Rumpel did create a Laravel package for adding missing assertions to the PhpUnit/Pest testing frameworks](https://github.com/christophrumpel/missing-livewire-assertions)

1. When requiring it is possible to get useful  assertion methods like `assertPropertyWired('nameOfProperty')`, `assertMethodWired('nameOfMethod')` (or for magic methods `assertMethodWired('$toggle(\'sortAsc\')')`)

1. It is advised to require the package in every TALL stack application (`composer require christophrumpel/missing-livewire-assertions`)

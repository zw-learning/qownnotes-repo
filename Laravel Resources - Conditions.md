Laravel Resources - Conditions
========================

## Hiding A Property

### `when(...)`

1. `when(...)` is used to check for if a given condition returns `true`

### `whenLoaded(...)`

1. `whenLoaded(...)` is used to check for if `with`is used / Model relationships are loaded

1. Using `whenLoaded(...)` can lead to the expected result, as soon as the condition is returning `false`/ `null`

    ![qownnotes-media-hbgwni](media/qownnotes-media-hbgwni.png)

1. There are cases, where the conditional can be used more readable

    ![qownnotes-media-MuzViH](media/qownnotes-media-MuzViH.png)

## Hiding Array Entries

### `can(...)`

1.  Single Array entires can also be hidden

1. `can(...)`is another way to solve this approach, while also utilizing Laravel Policies

    ![qownnotes-media-wAkGqv](media/qownnotes-media-wAkGqv.png)

Alpine.js - A General Overview
========================

1. Having gone through the horrible #neuland times with web technologies/approaches like jQuery and Bootstrap, it is nice to see a simple, modern JavaScript Framework that is alternative more in line with the Web Component approach, like Vue.js

1. Alpine.js, the real and only `x` you will ever need in your life, as it has much to offer, like simply declaring a block as a Alpine.js component via `x-data`, binding properties (via `x-bind`) or two-way binding objects (via `x-model` ) to HTML attributes, showing or hiding HTML elements with `x-show` or `x-cloak` or using `x-ref` to reference easily to a specific element or component via the `$refs` magic method

1. Not only that, but it also features like it's own outright (global) state management via `$store`, referencing the current active component via `$el`, `$dispatch` for event dispatching, `@` (as a shorthand for `x-on:`) for listening for events and `$watch` for changes to a property

1. It's no lie to say that Alpine.js is easily one of the most powerful lightweights out there, and it feels very similar to JavaScript frameworks like Vue.js, but with another scope and approach

## Ease Of Use

1. For more advanced features and topics, it is advised to check the articles about [CSP](https://alpinejs.dev/advanced/csp), [reactivity](https://alpinejs.dev/advanced/reactivity), [extending the core Alpine.js experience](https://alpinejs.dev/advanced/extending) and [async behaviour](https://alpinejs.dev/advanced/async) in detail

1. Also, existing plugins like [Mask](https://alpinejs.dev/plugins/mask),  [Anchor](https://alpinejs.dev/plugins/anchor), [Focus](https://alpinejs.dev/plugins/focus) and [Morph](https://alpinejs.dev/plugins/morph) should be looked up as well, due to their special use-case

### Initialization And Data Storage

1. It's as easy as defining a piece of HTML as a Alpine.js component via `x-data` directive / HTML attribute

    ![qownnotes-media-YqwIiP](media/qownnotes-media-YqwIiP.png)
    
    > How awesome and slim is that? The counter is ready!

1. Reactive handling is expected of all the defined properties within `x-data`, but it is also possible to left `x-data` stateless

    ![qownnotes-media-aOvOBk](media/qownnotes-media-aOvOBk.png)

1. `x-data` can also be defined as a multiline object within the HTML, but it is *heavily* advised to store all that defines a Alpine.js component within its own bundle/module/component/file

1. It is also possible to scope Alpine.js components in one another and also override the parent's values, *nested data*, so to say

    ![qownnotes-media-UMjVOo](media/qownnotes-media-UMjVOo.png)

1. Besides properties to keep track of, `x-data` can also take in methods and *getters* (similar to `computed`, but **not** cached(!) ([MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/get))) that can be used within an Alpine.js component

    ![qownnotes-media-ZrTlNq](media/qownnotes-media-ZrTlNq.png)

1. They can be used like any other method you would use within the landscape of Vue.js

    ![qownnotes-media-xBXBLX](media/qownnotes-media-xBXBLX.png)

1. Instead of expecting to set `x-data` only on more general HTML tags like `div`, `article` or `section`, they can also be bound to other HTML tags, if those Alpine.js components would then be "single element" components

    ![qownnotes-media-ckejCd](media/qownnotes-media-ckejCd.png)

#### Computed Properties / Getters

1.  Getters can be denoted with a `get` before their method name and this object/class scoped behaviour is naturally also supported by Alpine.js

    ![qownnotes-media-psAWbG](media/qownnotes-media-psAWbG.png)

1. Due to it being reactive, we can access `x-data` defined properties a bit differently from what one might expect in plain JavaScript

    ![qownnotes-media-MMHvTk](media/qownnotes-media-MMHvTk.png)

#### `Alpine.data()`

1. In case the properties stored within`x-data` are getting to cumbersome it is always possible to instead refer to `Alpine.data(...)`

1. `Alpine.data(...)` is the global data storage and can easily be initialized by listening globally to the `alpine:init` event

    ![qownnotes-media-kljctV](media/qownnotes-media-kljctV.png)

1. Not only that, it is also possible to import/use JavaScript bundles/modules

    ![qownnotes-media-kdKxWP](media/qownnotes-media-kdKxWP.png)

1. Alternatively parameters can be given over, as such bundles/modules are used as functions

    ![qownnotes-media-WBOjhi](media/qownnotes-media-WBOjhi.png)

1. The `Alpine.data()`global is also able to use the Alpine.js lifecycle hook `init`

    ![qownnotes-media-gwrDsB](media/qownnotes-media-gwrDsB.png)

1.  Similarly, it is able to acces magic properties via `this`, like `this.$watch`, and react to events on the bound/assigned Alpine.js

    ![qownnotes-media-LbHHsg](media/qownnotes-media-LbHHsg.png)

#### `Alpine.store()`

1. Alpine.js features it's own data store / state management, which is one step up from what Vue.js is covering by default, for which Vuex/Pinia can be used instead

1. Like with `Alpine.data()`, `Alpine.store()` can be initialized after `alpine:init` was triggered and Alpine.js is ready

    ![qownnotes-media-PcWtFP](media/qownnotes-media-PcWtFP.png)

1. It is possible to access the stores via the `$stores` magic property

    ![qownnotes-media-iPQobe](media/qownnotes-media-iPQobe.png)

1. Also, the use of the `init()` lifecycle hook is available within a store

    ![qownnotes-media-qRHfBU](media/qownnotes-media-qRHfBU.png)

1. Differing from what Vuex/Pinia are intending, `Alpine.store()` can also be used as a single-value store

    ![qownnotes-media-oitqUR](media/qownnotes-media-oitqUR.png)

### Listening And Working With Events

1. `@` (as a shorthand for `x-on:`) can be used to listen for events

    ![qownnotes-media-dnZTvM](media/qownnotes-media-dnZTvM.png)

1. All available [default listeners are listed within the official documentation](Alpine.js%20-%20A%20General%20Overview.md), they are very similar to what can be done with Vue.js

1. [Keyboard](https://alpinejs.dev/directives/on#keyboard-events) and [mouse events](https://alpinejs.dev/directives/on#mouse-events) are probably the more seldom used ones

1. [Custom Events](https://alpinejs.dev/directives/on#custom-events) however should be considered the typical most used ones

    ![qownnotes-media-nVzuMX](media/qownnotes-media-nVzuMX.png)

#### Component To Component

1. Equal to what is written in the [Livewire notes](Laravel%20Livewire%20-%20Component%20Nesting.md), events can be send via `$dispatch(...)` to specific component, which is not necessarily a child Alpine.js component

    ![qownnotes-media-zHAaBj](media/qownnotes-media-zHAaBj.png)

1. `$dispatch(...)` can also be used to trigger updates within an `x-model` directive, used for two-way data binding

    ![qownnotes-media-NvqbyA](media/qownnotes-media-NvqbyA.png)

#### Event Modifiers

1. There are also the typical [event modifiers](https://alpinejs.dev/directives/on#modifiers) that one might know [from Vue.js](https://vuejs.org/guide/essentials/event-handling.html#event-modifiers) already, but there are even more, as the scope of Alpine.js differs from 

1. `.outside`,  `.debounce`, `.throttle` might be the more used ones, while there is also `.window` for references to exclusive event listening on the `window` object

1. `.passive` for [setting up passive event listening](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener#improving_scrolling_performance_with_passive_listeners) is a special case, as `.prevent` should not be used in combination with it

1. For `.capture` there is an [extensive documentation](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener#usecapture) assigned to it, but it's also similar to what is refered to in Vue.js ([MDN](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener#usecapture))

### Modifying Content

1. It is possible to use `x-text` to simply change the text within an HTML element

    ![qownnotes-media-WBWQPA](media/qownnotes-media-WBWQPA.png)

1. Similarly, it is also possible to use `x-html` to change HTML within an HTML element, but - as always with this - it is advised against this, as it is a major security risk allowing for this

    ![qownnotes-media-rOuJwb](media/qownnotes-media-rOuJwb.png)

#### Toggling And Displaying Of Content

1. Referencing the `.outside` event modifier in combination with `x-data` and `@`, things like a *dropdown* or a *modal* can be created quite simply within Alpine.js

    ![qownnotes-media-dDyyFB](media/qownnotes-media-dDyyFB.png)

1. Same with showing (`x-show`) or hiding (`x-cloak`) of HTML elements where also the `x-transition` directive can be used to make for a more smooth usage

    ![qownnotes-media-WdrzoY](media/qownnotes-media-WdrzoY.png)

1. Differing from how `v-if` works for Vue.js `x-cloak`, which is just an alternative to `x-if="true"`, will force the additional CSS to be set instead (`[x-cloak] { display: none !important; }`)

#### Filtering Content

1. Alpine.js also makes it quite easy to filter existing content and apply the result temporarily within the DOM

    ![qownnotes-media-kCBwUY](media/qownnotes-media-kCBwUY.png)

#### The Teleporter

1. Similar to how Vue.js is handling teleports, Alpine.js is also handling this originally tough customer quite nicely, also with events and nesting

    ![qownnotes-media-QGDTxL](media/qownnotes-media-QGDTxL.png)
    
    ![qownnotes-media-qOCAQc](media/qownnotes-media-qOCAQc.png)
    
    ![qownnotes-media-NVdDTb](media/qownnotes-media-NVdDTb.png)
    
    ![qownnotes-media-OgHcRi](media/qownnotes-media-OgHcRi.png)
    
    ![qownnotes-media-bMpMNq](media/qownnotes-media-bMpMNq.png)

#### Identifying Yourself

1. The `x-id` directive is making heavily use of the `$id` magic property for assigning a`id` HTML tag to the current element scope, adding numeration automatically

    ![qownnotes-media-UaTtlZ](media/qownnotes-media-UaTtlZ.png)

1. This can also be used to group HTML elements, like input fields

    ![qownnotes-media-hyUiQD](media/qownnotes-media-hyUiQD.png)

1. *Comment*: This kind of behaviour, of manually naming and appending integers to every `id` tag name, might jog some memories as this approach was already famously used at least once in the past

1. Keying can also be effectively used in loops

    ![qownnotes-media-KVdoxX](media/qownnotes-media-KVdoxX.png)

### Binding To Inputs

1. Besides the `x-text` and `x-html`, there are also `x-bind` and `x-model`

1. While it can be assumed that `x-bind` could be used by `x-text` and `x-html`, this cannot be assumed for `x-model`, due to it's two-way data binding capabilities

    ![qownnotes-media-yBfTRu](media/qownnotes-media-yBfTRu.png)

1. Depending on the originating data type, values that are used within `x-model`, like for boolean and arrays, can result in automatic parsed, almost usable results

    ![qownnotes-media-Dazxen](media/qownnotes-media-Dazxen.png)
    
    ![qownnotes-media-PNdzBo](media/qownnotes-media-PNdzBo.png)
    
    ![qownnotes-media-KdtRyX](media/qownnotes-media-KdtRyX.png)
    
    ![qownnotes-media-AOGoBO](media/qownnotes-media-AOGoBO.png)
    
    ![qownnotes-media-HceSse](media/qownnotes-media-HceSse.png)
    
    ![qownnotes-media-OKLFgo](media/qownnotes-media-OKLFgo.png)
    
    ![qownnotes-media-peFEBi](media/qownnotes-media-peFEBi.png)

1. There are also [modifiers to the `x-model` two-way data binding](https://alpinejs.dev/directives/model#modifiers), like `.lazy`, `.number`, `.debounce`, `.boolean`, `.throttle`, `.fill` - most of them are probably known by now

1. In case the `x-model` must be approached from outside / via `$refs`, there is also an option for doing it easily, as there are `$el._x_model.get(...)` and `$el._x_model.set(...)` available

    ![qownnotes-media-NluLLV](media/qownnotes-media-NluLLV.png)

1. Also, if `x-model` should be the base of a change but the actual change should be written to another property `x-modelable` can be used

    ![qownnotes-media-dVyEOJ](media/qownnotes-media-dVyEOJ.png)

#### `Alpine.bind()`

1. It is also possible to bind a locally scoped `x-bind` on a more global level, to propose re-use

    ![qownnotes-media-vXZXUt](media/qownnotes-media-vXZXUt.png)

### Looping

1. The `key` to successful looping is to not forget *it*

    ![qownnotes-media-ySrEJa](media/qownnotes-media-ySrEJa.png)

1. `x-for` is quite similar to it's Vue.js couterpart, with the difference to the necessary `<template>...</template>` denotations

    ![qownnotes-media-WkvmhU](media/qownnotes-media-WkvmhU.png)
    
    ![qownnotes-media-vxMGiz](media/qownnotes-media-vxMGiz.png)

1. It is also possible to access the index within an `x-for`, like in Vue.js

    ![qownnotes-media-xlSiun](media/qownnotes-media-xlSiun.png)

1. No issues whatsoever when a more simple for loop is intended instead

    ![qownnotes-media-UvsdxM](media/qownnotes-media-UvsdxM.png)

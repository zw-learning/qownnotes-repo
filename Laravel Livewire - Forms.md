Laravel Livewire - Forms
========================

1. As a Livewire Component is basically just a Alpine.js Component, similar features and usability apply

1. This is also the case for handling forms, also being similar to what is happening for/with Vue.js

    ![qownnotes-media-ezuPWo](media/qownnotes-media-ezuPWo.png)

1. To make validation of submitted data possible, Livewire Components can also use the Laravel Validator

1. It is possible to prepend public variables with the PHP Annotation `#[Validate('...')] `and to [define simple validation types](https://livewire.laravel.com/docs/forms#adding-validation)

    ![qownnotes-media-caDVHf](media/qownnotes-media-caDVHf.png)

1. If the simple approach is not enough it is also possible to use the `rules()` method, to add and use Laravel's Rule Class

    ![qownnotes-media-dLeEcy](media/qownnotes-media-dLeEcy.png)

1. There are also options to do this within the Blade view / form, covering both sides of the rope

    ![qownnotes-media-HiCGgr](media/qownnotes-media-HiCGgr.png)

1. It is also possible to extract these validation efforts [into its own Class](https://livewire.laravel.com/docs/forms#extracting-a-form-object) under `App\Livewire\Forms`, extending from `Livewire\Form`

    ![qownnotes-media-pqxPCt](media/qownnotes-media-pqxPCt.png)

1. But not only is it possible to extract the validation, but also the Form request handling itself

    ![qownnotes-media-ABWpEv](media/qownnotes-media-ABWpEv.png)

1. Like it might already be expected, this can then be used via the `$this->form` variable

    ![qownnotes-media-aHITTb](media/qownnotes-media-aHITTb.png)

1. Extracting the form Class also makes it reusable for the *update*, instead of just the *store* workflow

1. The Livewire form Class has also, like the Livewire component, it's own `$this->reset()` method, which can reset the whole form with the click of, e.g., a button

1. Moreover, it is possible to use the `$this->reset()` method in a Livewire form Class, like in Livewire components

1. It is also possible to assign [*dirty* indicators](https://livewire.laravel.com/docs/forms#showing-dirty-indicators) to input fields

## Fields

1. It is also possible to use Blade components to reduce code repetition for field initializations

    ![qownnotes-media-spfrHU](media/qownnotes-media-spfrHU.png)

1. Remember how to define such a Blade component

    ![qownnotes-media-GuUVGu](media/qownnotes-media-GuUVGu.png)

## Custom Form Controls

1. For more about this its best to refer to the [official documentation](https://livewire.laravel.com/docs/forms#custom-form-controls)

    ![qownnotes-media-iwGOFa](media/qownnotes-media-iwGOFa.png)

## Loading Behaviour

1. It is possible to show a loading indicator on load

    ![qownnotes-media-wLLmnc](media/qownnotes-media-wLLmnc.png)

1. Similarly, it is already included to block additional form submitting on load

    ![qownnotes-media-iwXMla](media/qownnotes-media-iwXMla.png)

## Input Handling

### Live Updates

1. Instead of the need to submit a form, it is also possible to let fields update manually - **on each keystroke**

    ![qownnotes-media-XfmVVS](media/qownnotes-media-XfmVVS.png)

#### Throttling

1. Instead of sending a request on every keystroke there is also the option to throttle the saving, also if debouncing is not the way to go

    ![qownnotes-media-RjjWzH](media/qownnotes-media-RjjWzH.png)

#### Validation And Saving

1. It is also possible to [directly validate user input](https://livewire.laravel.com/docs/forms#real-time-validation)

1. This can be done via the `@error('...')`/ `@enderror` Blade directives

1. Real-time saving is one 

#### Using Livewire's `Updated()` Lifecycle Hook

1. It is also possible to use Livewire's `update()` lifecycle hook in a Livewire component [to catch field updates](https://livewire.laravel.com/docs/forms#real-time-form-saving)

### Blur Fields

1.  When *live updates* do seem to be a *bit* too network/resource heavy, there is also the option to "blur"

    ![qownnotes-media-OiVjtN](media/qownnotes-media-OiVjtN.png)

1. Only clicking away or tabbing will now sent a request to the Backend

### Debounce

1. If neither of the above seem lucrative a debounced behaviour might do the trick

    ![qownnotes-media-fngoFS](media/qownnotes-media-fngoFS.png)


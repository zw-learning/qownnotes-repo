Pest - Extending Expectations
========================

1. Pest defines a core feature set of available expectations, but as dynamically as it is, it's also as easily extendable

1. If an existing expectation should be negated, it can be prepended with a `->not`

1. Doing so makes the `loginAs(...)` method available everywhere within the tests - quite nice!

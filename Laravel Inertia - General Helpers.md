Laravel Inertia - General Helpers
========================

## Head

### Vue.js

1. Inertia has a `Head` Component that you can use within Vue.js to manipulate the HTML `<head>` area

1. This way things like the `canonical` link relation can be defined with much ease, as Inertia is pushing these information automatically to the top

    ![qownnotes-media-arKegQ](media/qownnotes-media-arKegQ.png)

1. Search engines are happier with this, ignoring indexing URL params, that are attached to the path

## Link 

### Vue.js


1. You can use the `Link` Component within Vue.js

    ![qownnotes-media-iNARxB](media/qownnotes-media-iNARxB.png)

1. Adding the attribute `preserve-scroll` will fix the jumpy navigation after clicks

1. The `only` attribute can save lives, as it can communicate from the Frontend which information Inertia really wants from the information of the Backend

    ![qownnotes-media-WfdDIo](media/qownnotes-media-WfdDIo.png)

1.  The usage of Closures within the  `inertia(...)` helper method will also be solving issues with unnecessary requests to the database, where nothing would be returnable, if not requested

    ![qownnotes-media-ZAJiFY](media/qownnotes-media-ZAJiFY.png)



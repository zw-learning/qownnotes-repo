Adding A Progress Bar To Your Job Execution
========================

1. You simply need to use the `progress(...)` helper method within a Job to get this feature up and running

    ![qownnotes-media-mAOevE](media/qownnotes-media-mAOevE.png)

1. The result will show within your terminal

    ![qownnotes-media-UIxHwD](media/qownnotes-media-UIxHwD.png)


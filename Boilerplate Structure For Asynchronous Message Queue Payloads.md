Boilerplate Structure For Asynchronous Message Queue Payloads
========================

1. Working with Microservices and Asynchronous Eventing is a great benefit as well as a great problem

1. The asynchronous parts are keen to approach issues with service availability and re-scheduling (timeout, retries, "back-off" periods)

1. However, there are ways around tackling this issue, if key facts are respected and extended on

 1. This is a boilerplate of key information for enriching such payloads manually

| key/reference | description | note |
|---|---|---|
| "payload" | the actual payload of the message | - |
| `sequenceId` | Can refer to a *version*, *process* or *event* ID | Different from `chainId` |
| `createdAt` | Refers to the creation date of the event | - |
| `updatedAt` | Refers to the last updated date of the event | - |
| `channelId` | Refers to the channel on which the event is send (client-subscriber-subscription-queue) | - |
| `groupId` | Refers to the queue name that is used within queue | - |
|` chainId` | Refers to the ID within the current workflow / batch | Different from `sequenceId` |
| `userId` | Refers to which user did trigger the request | for obfuscation reasons it might make sense to use `uuid`s here |

> If you have forgotten why `chainId` and `sequenceId` are different from one another take another deep-dive into [Microservices with Node JS and React (Udemy)](https://www.udemy.com/course-dashboard-redirect/?course_id=2887266) again ...
Pest - Assert Page Content
========================

## Content

### HTML

1. After sending an [request](Pest%20-%20Authentication%20And%20Routes.md)  it is possible to check for (non-)existing text

1.  Helper methods aree `assertSeeText([...])`, `assertDontSeeText([...])`, `assertSeeTextInOrder([])`

1. The helper method `assertSee()` can be used to check for files or URLs

1. There is also a helper method for `assertSeeLivewire(...)`, which can be used to check for specific components

### JSON


## Browser Tests
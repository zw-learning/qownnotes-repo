Pest - Datasets Usage With Laravel
========================

Using Pest in Laravel, you should first refer to [Pest Live Templates](Pest%20Live%20Templates.md) and   
[Using Laravel Factories Within Datasets](Pest%20-%20Using%20Laravel%20Factories%20Within%20Datasets.md)

## Datasets

1. You can simply use `->with([...])`on the helper method to describe a new test

    ![qownnotes-media-EsAVvr](media/qownnotes-media-EsAVvr.png)
    

1. Extending on that is also as easy as it seems

    ![qownnotes-media-klhdjq](media/qownnotes-media-klhdjq.png)

1. Another use-case could be iterating through possible values for validation expectations

    ![qownnotes-media-MpZHej](media/qownnotes-media-MpZHej.png)

## TDD

### Controller

1. It makes most sense to first use the `back()` Route helper method if you want to get started with evaluating an existing endpoint

### Models

#### Relationships

1. When requesting a Model always use `firstOrFail($id)`, where Laravel's route binding does not kick in beforehand
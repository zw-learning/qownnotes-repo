Laravel Authorization - General
========================

1. With Laravel 11 there can be an issue in regards to the `$this->authorize('permission', $className)`

1. When `$this->authorize(...)` is not available `Gate::authorize('permission', $className)` can be used synonymously
Database Limitations
========================

For *AMQPs* read also [Boilerplate Structure For Asynchronous Message Queue Payloads](Boilerplate%20Structure%20For%20Asynchronous%20Message%20Queue%20Payloads.md).

## MongoDB

### AMQP

1. There is a benefit to use MongoDB with JavaScript as basis of a Microservices, depending on Express.js

1. MongoDB already has a `_v` column set to each of it's stored JSON documents/objects

1. This helps much more than you would originally expect with automatic versioning in relation to AMQP between Microservices

## MySQL/MariaDB

### AMQP

1. With MySQL/MariaDB this is a bit more time consuming to do

1. Most likely a good approach would be to use `TRIGGER`s and using Laravel Model's life-hooks

1. Another option could be a`last_sequence_id` column as part of the payload, where the previous `sequence_id` is then written to

> Increasing or decreasing is not as safe as one might think, the Microservices will run into issues
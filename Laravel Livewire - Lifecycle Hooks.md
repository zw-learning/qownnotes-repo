Laravel Livewire - Lifecycle Hooks
========================

## Available Methods

### `mount()`

1. The Class method `mount(...)` replaces the `__construct(...)` method, so dependency injection is still available, but it is not limited to only the `mount(...)` lifecycle hook

1. In the same way as in regular Laravel Controllers, it can also access the Laravel Route params this way, either as Class or just the URI parameter, depending on your route setup

    ![qownnotes-media-tHLgKW](media/qownnotes-media-tHLgKW.png)

1. It is also possible to omit this part and let this part be handled automatically

    ![qownnotes-media-bMsYAO](media/qownnotes-media-bMsYAO.png)

1. `mount()` also symbolizes that it is only ever called once, on initialization

### `boot()`

1. As `mount(...)` only runs once, it might be of interest to execute certain actions and assigns on every request to the Livewire component in question

1. To do this, the `boot(...)` method comes into play, which can also dependency inject given parameters

1. Results are not cached though, as they will be forgotten and re/run on every new request

1. It is good to remember that there are also still *computed* methods available which might cover what is to be defined within a `boot(...)` method by a developer

    ![qownnotes-media-FBNsBH](media/qownnotes-media-FBNsBH.png)

### `update()` And `updated()`

1. When using, e.g., `wire:model` in the view of a Laravel component, the  `updating(...)` and `updated(...)` methods will be triggered

1. Besides accepting what is given over to the Livewire component by the view, `updating(...)` and  `updated(...)` also accept dependency injections

1. `updating(...)` is run before the `updated(...)` method to stop or authorize the change of the update in progress

1. In `updated(...)`  it is possible to manipulate the value given by the Livewire component's view before it reaches the processing to Event Sourcing or (directly) the database

1. In `updating(...)`, when assigning the PHP annotation `#[Locked]` to any property, Livewire automatically will stop update to that particular property, making it obsolete to do this manually

1. Instead of using `updated(...)`  it is also possible to use magic, custom Livewire lifecycle hooks, like `updatedPropertyName(...)`

    ![qownnotes-media-GKzVZU](media/qownnotes-media-GKzVZU.png)

1. Arrays are handled differently though, their requests contain an additional `$key` value

    ![qownnotes-media-vEZrbe](media/qownnotes-media-vEZrbe.png)

### `hydrate()` And `dehydrate()`

1. `hydrate(...)` and `dehydrate()` also accept dependency injections

An exclusive Copy+paste:

> Hydrate and dehydrate are lesser-known and lesser-utilized hooks. However, there are specific scenarios where they can be powerful.
> 
> The terms "dehydrate" and "hydrate" refer to a Livewire component being serialized to JSON for the client-side and then unserialized back into a PHP object on the subsequent request.
> 
> We often use the terms "hydrate" and "dehydrate" to refer to this process throughout Livewire's codebase and the documentation. 
> 
> If you'd like more clarity on these terms, you can learn more by [consulting our hydration documentation](https://livewire.laravel.com/docs/hydration).
> 
> Let's look at an example that uses both `mount(...)` , `hydrate(...)`, and `dehydrate(...)` all together to support using a custom [data transfer object](https://en.wikipedia.org/wiki/Data_transfer_object) (*DTO*) instead of an Eloquent model to store the post data in the component:
> 
> ![qownnotes-media-EwJbuB](media/qownnotes-media-EwJbuB.png)
> 
> Now, from actions and other places inside your component, you can access the `PostDto` object instead of the primitive data.
> 
> The above example mainly demonstrates the abilities and nature of the `hydrate(...)` and `dehydrate(...)` hooks. However, it is recommended that you use [*Wireables* or *Synthesizers*](https://livewire.laravel.com/docs/properties#supporting-custom-types) to accomplish this instead.

### `rendering()` and `rendered()`

1. `rendering(...)`and `rendered(...)` are also part of the Livewire lifecycle hooks and accepts also dependency injections

1. `rendering(...)` and `rendered(...)` both accept the properties `$view` and `$data`, both are in turn executed *before* and *after* the Livewire component's view is rendered (`render()`)

### `exception()`

1. Error handling is also part of the Livewire component's lifecycle and `excepion(...)` can also use dependency injection

1. Parameters, that are given to the `exception(...)` method by default are the `$error` itself and if the propagation should be stopped (`$stopPropagation`)

1. It is always advisable to stop and return early

1. This method can also be used to change the error message that is given back or ignore some of them entirely after all

1. This method is also used internally by, e.g., the `validate(...)` Livewire component method

## Using Lifecycle Hooks Within Traits

1. It is also possible to access the Livewire component's lifecycle hooks within a Trait, for extracting reused code (*or just to clean up your code*)

1. To avoid multiple Traits having the same named lifecycle hooks implemented, it is possible to use a magic method that needs to be written as `lifecycleHookTraitName(...)`

    ![qownnotes-media-thOrUs](media/qownnotes-media-thOrUs.png)
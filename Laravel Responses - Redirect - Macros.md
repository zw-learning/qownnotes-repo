Laravel Responses - Redirect - Macros
========================

1. *Marcos* are definable on any Class that uses the `Macroable` trait

1. Its greatest power is hidden within the defined `__callStatic(...)` and `__call(...)` magic methods as well as the `bindTo(...)` method 

1. *Macros* are just a shortcut for stopping to repeat code on and on, with simple chainable methods, whose name can be freely defined on assignment

1. Syntactic sugar is most of the time one of the key experiences for good developers working with any application

1. The definition for this on compatible Classes can be done within the `JetstreamServiceProvider` or any other rather Service Provider

    ![qownnotes-media-wNXCqj](media/qownnotes-media-wNXCqj.png)

1. Using it, a name followed by what payload should be carried over to the Frontend is necessary

    ![qownnotes-media-sGYBcP](media/qownnotes-media-sGYBcP.png)

1.  In the Frontend it could then show up like this

    ![qownnotes-media-rFcGvh](media/qownnotes-media-rFcGvh.png)

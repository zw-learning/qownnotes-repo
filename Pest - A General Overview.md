Pest - A General Overview
========================

## Excerpt

> A complete suite of green tests are the best feeling in backend development

1. Pest is written "Pest" - no use screaming its name

1. Pest is very similar in writing to Jest or cypress

1. Pest can be easily supported within PhpStorm due to an 1st party plugin

1. Pest is a layer on-top of PhpUnit

1. Pest feels very lightweight in usage because assigning to classes will be handled by its package

## Setup

### Laravel

1. There is a difference in *Unit* and *Feature* tests

1. While the *Feature* tests are setup to boot the Laravel application, this is not the case for *Unit* tests

1. This is defined within the `tests/Pest.php` file, where it is also possible to [create custom macros / helper functions](Laravel%20Responses%20-%20Redirect%20-%20Macros.md)

    ![qownnotes-media-ksuhYX](media/qownnotes-media-ksuhYX.png)

1. *Unit* tests are in fact those that do test only one specific thing, lightning fast due to being lightweight, but booting up Laravel still seems necessary for even the simplest tests

## Writing A Test    

1. Pest tests can be defined as `test('name')` or `it('description')`, both are valid

    > Here it does make sense to add _Live Templates_ within PhpStorm to ease the usage
    > and for more on this check [Pest Live Templates](Pest%20Live%20Templates.md)


1. In Pest `assert()` and `expect()` are two helper methods that are key to successfully written tests

1. Pest has support for Data-providers / Datasets, you can find a more through explanation [here](Pest%20-%20Using%20Laravel%20Factories%20Within%20Datasets.md)

## Testing Lifecycle

1. Pest provides `setUp()` and `tearDown()` helper methods which are each used before and after a test case is run

    1. Along with those there are also `beforeEach()` and `afterEach()`, which are also part of the lifecycle and run between tests

## Routing

1. In Pest to use a specific authorized "logged in" User, simply using the `actingAs($user)` helper method will cover this

1. There are many helper methods that have been introduced to Pest for a more fluent routing experience, see [Pest - Authentication And Routes](Pest%20-%20Authentication%20And%20Routes.md)

## Plugins

1. It makes sense to make Pest able to understand the `faker()` helper method (`composer require pestphp/pest-plugin-faker --dev`)

1. Also, while PhpStorm is already giving this feature basically by default, extending Pest to watch for code changes via CLI may also be useful for everyone (`composer require pestphp/pest-plugin-watch --dev`)

1. Related to the context of testing, the Livewire plugin may also be of interest (`composer require pestphp/pest-plugin-livewire --dev`)
Laravel Controllers - Creation Titbits
========================

## Morphed Creation

1. After the [Laravel Policies - Creation Titbits](Laravel%20Policies%20-%20Creation%20Titbits.md) is handled the next step is probably the Controller

1. Checking within the Controller if expectations do not match should return something with Status Code 404, in the JSON Payload / Response

    ![qownnotes-media-hQHwhB](media/qownnotes-media-hQHwhB.png)

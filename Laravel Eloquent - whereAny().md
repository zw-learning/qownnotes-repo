Laravel Eloquent - whereAny()
========================

1. Recently Laravel has implemented a `whereAny([...], ...)` helper method

1. This replaces `->where(...)` + `->orWhere(...)` chains when, e.g., searching for a text in several columns

    ```
    // old
    $model->where('title', 'LIKE', '%' . $request->query('query') . '%)
          ->orWhere('body', 'LIKE', '%' . $request->query('query') . '%)
    
    // new
    $model->whereAny(['title', 'body'], 'LIKE', '%' . $request->query('query') . '%)
    ```

1. This is nice and all, but given a more advanced filtering approach is used, this might be incompatible with existing code
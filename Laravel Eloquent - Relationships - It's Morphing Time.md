Laravel Eloquent - Relationships - It's Morphing Time
========================

## Indexing

1. When creating the morphing columns using `$table->morphTo(...)` you should absolutely keep in mind to create a `unique` index key

1. The `unique` index key should refer to the entry's `id`  *x*`able_id` and *x*`able_type`  columns


Laravel Livewire - Component Nesting
========================

1. As already gone into within the depths of [Laravel Livewire - A Component](Laravel%20Livewire%20-%20A%20Component.md) it is possible to nest Livewire/Alpine.js components into another

1. While this is possible, it is also a very reasonable question if a normal Blade component wouldn't also be good enough, as Alpine.js is automatically available anyway

    ![qownnotes-media-AFdTfd](media/qownnotes-media-AFdTfd.png)

1. For a more technical understanding it is advised to [check out their extensive article about this topic](https://livewire.laravel.com/docs/understanding-nesting), on how to decide what's considered wrong or right

## Actual Nesting

1. The nesting is as easy as including another Livewire component inside the current one 

    ![qownnotes-media-zqOoPR](media/qownnotes-media-zqOoPR.png)

1. It is as usable as with Blade components

1. Similar results are given when properties are in need of being given over to a child component

    ![qownnotes-media-VBCRbE](media/qownnotes-media-VBCRbE.png)
    
    > Remember that it is advised to omit the `render(...)` method in favour of publicly defined properties in the Livewire component's Class scope to save code repetition

1. There is still the need to assign the PHP annotation `#[Reactive]` to any property that should be reactive and forward updates to child components

1. A similar approach needs to be used if a Livewire child component should be able to *wire* the property of a parent as a model (assign `#[Modelable]` to the property), **but there are caveats to be aware of as well**

    ![qownnotes-media-XNDujN](media/qownnotes-media-XNDujN.png)
    
    ![qownnotes-media-cIxAoZ](media/qownnotes-media-cIxAoZ.png)
    
    ![qownnotes-media-Puuodf](media/qownnotes-media-Puuodf.png)

1.  It is also possible to parse static values and use shorthand connotations

    ![qownnotes-media-HifATL](media/qownnotes-media-HifATL.png)

1. It is also possible to `@foreach` over Livewire child components (do not forget to set and fill a `key` attribute, like in Alpine.js/Vue.js)

    ![qownnotes-media-BeJnNp](media/qownnotes-media-BeJnNp.png)

1. It is also possible to walk over elements recursively

    ![qownnotes-media-ThYtWZ](media/qownnotes-media-ThYtWZ.png)

## Re:Rendering

1. [Nested Livewire components are islands](https://livewire.laravel.com/docs/understanding-nesting#every-component-is-an-island), so it is useful and sometimes necessary to re-render child components

1. To do this it is advised to always provide a `key` attribute to a child component, no matter if they are *not* used within a loop/iteration

### Magic Variable Methods

1. `$parent` allows any Livewire child component to access its parent's feature set

    ![qownnotes-media-ZTiuRA](media/qownnotes-media-ZTiuRA.png)

1. Plain *wizardry* - to dynamically load a given component wiring via `livewire:dynamic-component :is="$current"` (or `<livewire:is :component="$current" />`) can be used

    ![qownnotes-media-gPHsdI](media/qownnotes-media-gPHsdI.png)



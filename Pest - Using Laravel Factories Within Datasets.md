Pest - Using Laravel Factories Within Datasets
========================

1. Remember, within Pest and when using Laravel Factories, for Datasets it's important to wait for Laravel to be successfully "booted"

1. To do this, the usage of a Closure is necessary

    ![qownnotes-media-GmZzxi](media/qownnotes-media-GmZzxi.png)

1. If this is done correctly, it will be smooth sailing, as the Laravel Application/Container is already ready
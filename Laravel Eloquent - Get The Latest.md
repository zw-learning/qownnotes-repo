Laravel Eloquent - Get The Latest
========================

1. The `latest()` helper method can actually be chained on and on

1. The idea being to create a prioritization in which latest sorting should come first

1. This is the same as raw SQL, but still an interesting titbit

1. For example sorting by `created_at`, then by `id` can be as easy as the following few lines

    ```
    $model->get()
        ->latest()
        ->latest('id')
    ```
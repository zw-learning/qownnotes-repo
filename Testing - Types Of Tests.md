Testing - Types Of Tests
========================

## Integration Tests

1. Integration tests are what define tests that go over the boundaries of different scopes, like (Micro)Services

1. They should test if expected workflows are run through successfully while also the expected events and values are being send, listened to and contained

## Feature Tests

1. Feature tests are checking different scopes of one application

1. Everything that is bigger than a unit test can also be automatically considered a feature test

1. Sadly, the line between what a feature test is and what a unit test is differs (heavily) based on the person you ask, their experience and/or the community their are part of


## Unit Tests

1. Unit tests are the smallest tests that can be written

1. They should just check one specific thing and assert or expect that it works

1. Note: Some developers go heavily by that unit tests that hit the database are feature tests, even though you just want to check the creation of any object

## Aliases

### Smoke Tests

1. Smoke tests are feature or unit tests that are just checking if there are no catastrophic issues on general execution

1. They don't check specifics, they are just go *poof* and are already gone

1. *A penny saved is a penny got*

### Black-box Tests



### White-box Tests
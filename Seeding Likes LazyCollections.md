Seeding Likes LazyCollections
========================

1. Remember that using LazyCollections can help massively with memory limit restrictions

    ![qownnotes-media-Kuodbz](media/qownnotes-media-Kuodbz.png)
    
    > If you are using an incremental table column, do not forget to check [Adding A Count By Default To Relationships](Laravel%20Models%20-%20Adding%20A%20Count%20By%20Default%20To%20Relationships.md).
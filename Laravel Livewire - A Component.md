Laravel Livewire - A Component
========================

1. Namespace is in `App\Http\Livewire` and each Class extends from `Livewire\Component`

    ![qownnotes-media-NtLwPe](media/qownnotes-media-NtLwPe.png)

1. It might also make sense to refer to [these Laracasts video series](https://laracasts.com/topics/laravel-livewire)

## Lifecycle Hooks

1. Or, *live cycle* - like many other technical terms it is not a 100% clear how to write this one ...

1. It covered in [Laravel Livewire - Lifecycle Hooks](Laravel%20Livewire%20-%20Lifecycle%20Hooks.md)

1. This is the [full list in the official documentation](https://livewire.laravel.com/docs/lifecycle-hooks)

1. Lifecycle hooks are used all over the Livewire component and they majorly reflect the thoughts and possibilities given by [Web Components](https://developer.mozilla.org/en-US/docs/Web/API/Web_components)

1. All lifecycle hook methods need to be defined as *public*, of course

## Rendering

1. Livewire components include a `render()` method that refers to a (`.blade.php`) view (file) located under `resorces/views/livewire` which is then parsed and evaluated

    ![qownnotes-media-FUuvyp](media/qownnotes-media-FUuvyp.png)
    
    ![qownnotes-media-DKBBIx](media/qownnotes-media-DKBBIx.png)
    
    ![qownnotes-media-zZSFjg](media/qownnotes-media-zZSFjg.png)

1. To use Livewire components it is also necessary to include/use the  `@livewireStyles` and `@livewireScripts` Blade directives within a (layout) file of your Blade view

    1. Remember, the default layout file is stored under `resources/views/components/layouts/app.blade.php`

1. To use variables within any Livewire component they need to be defined as `public` within the component Class or attached via `with([])` on the `view()` helper method

1. It is also possible to define a Livewire component as inline, this will change the content within the `render()` method

    ![qownnotes-media-HRGacT](media/qownnotes-media-HRGacT.png)

1. It is possible to include a Livewire component, like any Blade component, inside any view via `<livewire:name-of-the-component /> ` or `@livewire('name-of-the-component')`

1. Actually, *kebab-case* is forced here, when including a Livewire component

1. If the main content of a page is a Livewire (full-page) component, it can be passed directly into a Laravel Route as if it were a Laravel Controller

    1. Livewire will render the Livewire component into the `{{ $slot }}` of a Blade layout component

1. It is also possible to skip re-rendering, which can also be done by defining the `#[Renderless]` PHP Annotation on an action or by using `$this->skipRender()`

## Layout & Metadata

1. One last quick note about layouts, they can be defined per (full-page) Component

    ![qownnotes-media-qFxQXF](media/qownnotes-media-qFxQXF.png)

1. Metadata like the page title can be defined via PHP Attribute notations and like the layout it can be defined on the Class or `render()` method scope

    ![qownnotes-media-BlTQRu](media/qownnotes-media-BlTQRu.png)

1. For more options it is a good idea to read up in the [official documentation](thttps://livewire.laravel.com/docs/components#setting-additional-layout-file-slots)

## Handling Properties

1. Components are able to initialize, bind and set properties, while also having some reactivity to them

1. To declare a property as reactive it is necessary to add the PHP annotation `#[Reactive]`, so any child Livewire component also gets notified about a change to the given property

1. A similar approach needs to be used if a Livewire child component should be able to *wire* the property of a parent as a model (assign `#[Modelable]` to the property)

    > Doing so will result in caveats though, seen within [Laravel Livewire - Component Nesting](Laravel%20Livewire%20-%20Component%20Nesting.md)

1. It makes sense to define them on a more global level to also save the use of the `mount(...)` lifecycle hook method

    ![qownnotes-media-KcLxNA](media/qownnotes-media-KcLxNA.png)

1. For information about which data types are supported a look into the [official documentation](https://livewire.laravel.com/docs/properties#supported-property-types) will help

1. It is also possible to assign the PHP annotation `#[Url]` to a property, to make it respect URL query parameters

### Wireables

1. Also, there is the option to implementing the `Wireable` interface and adding the `toLivewire()` and `fromLivewire()` methods

### Reset

1. Not only that, but there is also a method as part of the `Livewire\Component `Class, that is able to `$this->reset(...)` such

    ![qownnotes-media-QKUuLi](media/qownnotes-media-QKUuLi.png)

1. But this comes with something to think about

    ![qownnotes-media-wcptTE](media/qownnotes-media-wcptTE.png)

### Pull

1. In cases where a reset and a re-initialization are in need, `$this->pull(...)` can be used instead

    ![qownnotes-media-BqaBPm](media/qownnotes-media-BqaBPm.png)

### Magic Variables

1. The full list can be seen within the [official documentation](https://livewire.laravel.com/docs/actions#magic-actions)

1. Still, currently there are `$parent`, `$set`, `$refresh`, `$toggle`, `$dispatch` and `$event` which can also be used via Alpine.js

## Response

1. It is also possible to modify any response that will be send from the Livewire's `render()` method

1. For this custom header and such can be set per (full-page) Livewire component

    ![qownnotes-media-lWGuXZ](media/qownnotes-media-lWGuXZ.png)

## Binding Via HTML Tag Attributes

1. When including Livewire components in any Blade view, it is possible to bind variables to such

    ```
    // one way
    <livewire:show-post :post="$post">
    
    // or another
    @livewire('show-post', ['post' => $post])
    ```
    
    ![qownnotes-media-uiDOnu](media/qownnotes-media-uiDOnu.png)

1. Reactivity can be a cause for concern here

    ![qownnotes-media-lfuuuv](media/qownnotes-media-lfuuuv.png)

1. Remember, such variables are required to be `public` or first forwarded to the view via chaining (`view()->with([...])`) to be useable in any Component view
 
1. Like with Vue.js/Alpine.js it is also necessary to bind a `:key` attribute on a `@foreach` loop

    ![qownnotes-media-Pkljba](media/qownnotes-media-Pkljba.png)

1. Similarly there is model binding for input fields, but in these cases we do not need to use `$` for the variable in use

    ![qownnotes-media-iXDYuW](media/qownnotes-media-iXDYuW.png)

1. When expecting live changes based on inputs, it is still necessary to keep in mind, that changes are only temporary until an *action* is executed to the Backend (**or** if the `:live` pseudo attribute is used)

    ![qownnotes-media-dgbplh](media/qownnotes-media-dgbplh.png)

## Computed

1. It is possible to define a method as a *computed*

1. It can be done by setting an PHP Attribute `#[Computed]` to the method that should be defined as a *computed*

1. Also, *computed* are cached and have a performance advantage

## Actions

1. *Actions* can be understand as simple methods, but they are stored and executed within a Livewire component

    ![qownnotes-media-soXfpE](media/qownnotes-media-soXfpE.png)

1. It is still possible to use regular helper methods like within Laravel Controllers like the `redirect()` helper method.

## Caveats Of The Script And Asset Directive

1. Below the two topics will be run through

1. There are some caveats to using them in Livewire v3 and below, though

    ![qownnotes-media-KriTow](media/qownnotes-media-KriTow.png)

## Scripting

### JavaScript

1. Being inside the View of a Livewire component it is also possible to use JavaScript

1. For this, there is the known `@script` and `@endscript` Blade directives, which are also supported by Livewire

    ![qownnotes-media-SdvAus](media/qownnotes-media-SdvAus.png)

1. It is also possible to natively use Alpine.js within Livewire Components (TALL stack), as they are essentially the same

    ![qownnotes-media-uLUFMN](media/qownnotes-media-uLUFMN.png)

1. Not only that, but it is essentially as well possible to write JavaScript within a PHP Livewire component, although it can be argued against doing so ... 

    ![qownnotes-media-GtRNYy](media/qownnotes-media-GtRNYy.png)
    
    > Note that this does not trigger an action to the Backend

1. It is also possible to use its underlying `$this->js(...)` method within the scope of a normal method

    ![qownnotes-media-VHBaeX](media/qownnotes-media-VHBaeX.png)

### Refreshing Components

1. If the need occurs, a Component can also be refreshed by using the `$refresh` magic variable (method), which refers internally to `$commit`

1. It is also possible to use this within Alpine.js

    ![qownnotes-media-RbvIFp](media/qownnotes-media-RbvIFp.png)


### Wiring Between Component Class And View

1. The magic variable `$wire` is exposed to any Component view, so that it is possible to use everything defined as public variable within a Livewire component

#### Getters

1. To access these variables it is possible to either use `$wire.propertyName` or `$wire.get('propertyName')`, whatever is prefered

    ![qownnotes-media-rxtFSx](media/qownnotes-media-rxtFSx.png)
    
    ![qownnotes-media-WyDzOl](media/qownnotes-media-WyDzOl.png)

#### Setters

1. As probably expected, it is also possible to manipulate date via `$wire.propertyName = value` and `$wire.set('value')`

1. This doesn't automatically send a request to the server, though, so it is necessary to think about creating some Livewire Action to persist the data manipulation

    ![qownnotes-media-RkYKIA](media/qownnotes-media-RkYKIA.png)

1. This is worked around via using the `$wire.set(...)` method, as it will - by default - trigger a network request  (can be disabled)

    ![qownnotes-media-WPyWrx](media/qownnotes-media-WPyWrx.png)

## Validation And Security Concerns

1. Like always, do never trust values given over by the Frontend ... or another API, for that matter

1. Being able to "just" update and set values can be a major cause of validation issues

1. Check out the [security concerns for properties](https://livewire.laravel.com/docs/properties#security-concerns) in full detail to not miss out on something important, also in regards to [security concerns with actions](https://livewire.laravel.com/docs/actions#security-concerns)

1. But still, using `#[Locked] `or Laravel (Route) Model binding also does not completely limit faulty usage

1. There is gladly still the option to use Laravel's full force of validation

1. It is possible to prepend public variables with the PHP Annotation `#[Validate('...')] `and to [define simple validation types](https://livewire.laravel.com/docs/forms#adding-validation)

    ![qownnotes-media-caDVHf](media/qownnotes-media-caDVHf.png)

1. There are also options to do this within the Blade view / form, covering both sides of the rope

    ![qownnotes-media-HiCGgr](media/qownnotes-media-HiCGgr.png)

1. It is also possible to extract these validation efforts [into its own Class](https://livewire.laravel.com/docs/forms#extracting-a-form-object) under `App\Livewire\Forms`, extending from `Livewire\Form`

    ![qownnotes-media-pqxPCt](media/qownnotes-media-pqxPCt.png)

1. Extraction makes for good reusability

## Events

1. There are certain events that can be used to trigger effects, and third party events can also be handled well

1. The full list can be seen within the [official documentation](https://livewire.laravel.com/docs/actions#event-listeners)

1. It is possible to listen for any event quite easily

    ![qownnotes-media-ZRMqzi](media/qownnotes-media-ZRMqzi.png)

1. Form handling and reacting to the processing while saving is also [handled well](https://livewire.laravel.com/docs/actions#disabling-inputs-while-a-form-is-being-submitted)

1. For more on this check out [Laravel Livewire - Events](Laravel%20Livewire%20-%20Events.md)

## Assets

1. Including assets is also very similar to how it is done within native Blade

1. Assets and Scripting can also be combined, if the possibility did not already seemed to be clear

    ![qownnotes-media-mGgPyp](media/qownnotes-media-mGgPyp.png)



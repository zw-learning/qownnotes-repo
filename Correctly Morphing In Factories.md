Correctly Morphing In Factories
========================

1. Do yourself a solid and define a `Relation::enforceMorphMap([...])` within the `AppServiceProvider`

    ![qownnotes-media-KJRqWO](media/qownnotes-media-KJRqWO.png)

1. Get a method within your model factory ready, to get knowledge of the assigned `...able_id` parameter
 
    ![qownnotes-media-ZjRLPR](media/qownnotes-media-ZjRLPR.png)
 
 1. Assign the method within your model factory and unpack the current values accordingly

    ![qownnotes-media-coadhp](media/qownnotes-media-coadhp.png)


1. Witness the beauty of it working

    ![qownnotes-media-aBWxdx](media/qownnotes-media-aBWxdx.png)

    ![qownnotes-media-MWzbjD](media/qownnotes-media-MWzbjD.png)

1. Finally, make use of the morphing within your seeders

    ![qownnotes-media-pvVvSu](media/qownnotes-media-pvVvSu.png)
    
    > And do not forget to add `...` before `$model->random`, because it expects an `array`
    >
    > ![qownnotes-media-AfgdFo](media/qownnotes-media-AfgdFo.png)

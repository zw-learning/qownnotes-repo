Laravel Policies - Creation Titbits
========================

## Morphed Relationships

1. If you are working with a morphed  relationship, a unique identifier should be created, see also [Laravel Eloquent - Relationships - It's Morphing Time](Laravel%20Eloquent%20-%20Relationships%20-%20It%27s%20Morphing%20Time.md)

1. You should check for if there is already an existing unique Model with requested details

     ![qownnotes-media-dsamLS](media/qownnotes-media-dsamLS.png)

1. It would also make sense to additionally check for the supported morphed types

    ![qownnotes-media-EJNByf](media/qownnotes-media-EJNByf.png)

1. Returning a 404 in such cases might also be a valid approach

    
Pest - Authentication And Routes
========================

## Authentication

1. To authenticate with an User, in Pest you can simply use the `actingAs(...)` helper method

1. The authentication is easily done then

## Route Management

After logging in you can ...

1. Also define from where you are coming, based on the chained `fromRoute(...)` helper

1. Then chain helper methods like `get(...)`, ``post(...)`` or `put(...)`

1. This can eventually be followed up by, e.g., an `assertOk()`, `assertRedirect(...)` or `assertHasPaginatedResource(...)`

> How does Pest do this? See [Laravel Responses - Redirect - Macros](Laravel%20Responses%20-%20Redirect%20-%20Macros.md)
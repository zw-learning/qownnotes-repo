# qownnotes-repo

My public notes repository.

This is done based on QOwnNotes.

As its SQLite database is not uploaded, you, dear avid reader, will only see
loose and probably badly named Markdown files — a real mess.

For me this is just another place, another backup.

Screenshots assigned are mostly based on different official documentations,
screencasts, news article or code my own.

For example, very likely locations of screencasts:

- On Udemy.com
- On Laracasts.com
- On YouTube.com

Also, possibly from a news/regurgitated article from, but not limited to:

- On laravel-news.com
- On medium.com
- On hacker-news.com
- On stackoverflow.com

If anybody feels offended by me taking and using screenshots of your screencast,
article, etc., please reach out via raising an issue.

That makes it not forgotten to the internet - sorry -, but I can always rebase
instead of commiting a new state.

Also, this repository's history, I don't care much for.

Thanks in advance, and may the included notes also be of some benefit to you,
avid reader.

As it is licensed under MIT, extend, fork and share as much as you would like to
do.

Knowledge should not cost anything and should be freely available, is my
personal opinion.

Cheers!

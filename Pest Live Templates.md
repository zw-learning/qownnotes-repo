Pest Live Templates
========================

## it(...)

1. Add or amend the existing one into the following

    ```
    it('$NAME$', function() {
        /** Arrange */
        $END$
        
        /** Act */
        
        /** Assert */
    });
    ```

1. Make it applicable within the *Pest root* scope

1. Expand with Tab

1. Reformat it according to style
    
1. Finally, see it in action

    ![qownnotes-media-MFHVij](media/qownnotes-media-MFHVij.png)


## test(...)

1. Add or amend the existing one into the following

    ```
    test('$NAME$', function() {
        /** Arrange */
        $END$
        
        /** Act */
        
        /** Assert */
    });
    ```

1. Make it applicable within the *Pest root* scope

1. Expand with Tab

1. Reformat it according to style

1. Finally, see it in action

    ![qownnotes-media-JKrJhQ](media/qownnotes-media-JKrJhQ.png)

Laravel Models - Adding A Count By Default To Relationships
========================

## Option 1 (Bad Performance)

1. Add the `protected $withCount = ['relationshipName'];`to the Model

    ![qownnotes-media-vihikh](media/qownnotes-media-vihikh.png)

1. Add the value about it to your Model Resource

    ![qownnotes-media-vFUEld](media/qownnotes-media-vFUEld.png)

1. Expect to see the result

    ![qownnotes-media-TNczPY](media/qownnotes-media-TNczPY.png)
    
    > Do not forget that JSON API Specs define keys should be written in bump case (e.g. `createdAt` instead of `created_at`)

## Option 2 (Good; Database De-normalization)

1. Just add a column to the model itself will boost the efficiency of each request significantly, measured against *Option 1*

    ![qownnotes-media-qYAiQK](media/qownnotes-media-qYAiQK.png)
    
    > This makes it necessary to increment or decrement the value of the comment manually

1. The same step is still necessary to be applied to your Model Resource

    ![qownnotes-media-vFUEld](media/qownnotes-media-vFUEld.png)

1. Expect to see the result

    ![qownnotes-media-TNczPY](media/qownnotes-media-TNczPY.png)
    
    > Remember that JSON API Specs define keys should be written in bump case (e.g. `createdAt` instead of `created_at`)

1. On adding, you need now to perform an additional step, to increase the value for said added column

    ![qownnotes-media-yKuIgG](media/qownnotes-media-yKuIgG.png)
    
    > You can compare with [Seeding Likes LazyCollections](Seeding%20Likes%20LazyCollections.md) for more on handling Seeders.

> The performance uplift is much more beneficial than the risk of messing up the count and breaking the normalization rules

1. Another example for when this is executed within a Controller (no route binding available in this one)

    ![qownnotes-media-tvzFDg](media/qownnotes-media-tvzFDg.png)

1. You should use Laravel Model Lifecycle hooks / Event Sourcing to catch unexpected bugs